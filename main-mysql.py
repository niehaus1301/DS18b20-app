import os
import glob
import time 
import mysql.connector

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'

db = mysql.connector.connect(
  host="remotemysql.com",
  user="lL4CBga8AJ",
  passwd="cWuHOhXq19",
  database="lL4CBga8AJ"
)

sql = "UPDATE temp SET temperature = %s"

recent = 0

def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines
 
def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c

        
while True:
        temp = round(read_temp(), 1)
        print(temp)
        if (temp != recent):
                recent = temp
                db.cursor().execute(sql % temp)
                db.commit()
        time.sleep(1)
